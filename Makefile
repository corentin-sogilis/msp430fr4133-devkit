# SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS
#
# Makefile : Build Docker image for MSP430FR4133 development environment
# Author : Corentin <corentin@sogilis.com>, Alex <alexandre@sogilis.com>
#
# SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS

# Required files at root directory to build the docker image :
# - msp430-gcc-5.3.0.224_linux64.tar.bz2
# - msp430-gcc-support-files-1.194.zip
# - msp430.dll_developer_package_rev_3.08.001.000.zip
# - mspdebug (binary from mspdebug compilation, version >= 0.24 )

IMAGE_NAME = sogicoco/msp430fr4133-devkit

all : main

main : Dockerfile
	docker build -t $(IMAGE_NAME) .

run :
	docker run --rm --privileged -v $(shell pwd):/opt/workspace -it $(IMAGE_NAME) /bin/bash

clean :
	docker rmi $(IMAGE_NAME)

fetch_dependencies :
	mkdir vendor
	wget -P vendor http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/exports/msp430-gcc-5.3.0.224_linux64.tar.bz2
	wget -P vendor http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/exports/msp430-gcc-support-files-1.194.zip
	echo "To complete the bundle, you need to download the MSPDebugStack from : http://www.ti.com/tool/mspds to : vendor/ directory"
	echo "You also need to download and compile mspdebug version >= 0.24 from : https://github.com/dlbeer/mspdebug to : vendor/ directory"
	echo "To date, the best packaged version of mspdebug in Debian is 0.22. Hence the need for compilation"
