# Dockerfile for the MSP430FR4133 launchpad

## Goal

This Dockerfile aims to build a full development environment for the
Ti's MSP430FR4133 launchpad board.

Available tools in the container are:
- msp430-elf-gcc
- msp430-elf-gdb
- mspebug tilib

## Build

### Prerequisite

File | Desc | Link
-----+------+-----
msp430-gcc-5.3.0.224_linux64.tar.bz2                | GCC toolchain distribution from TI    | [link](http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/exports/msp430-gcc-5.3.0.224_linux64.tar.bz2)
msp430-gcc-support-files-1.194.zip                  | Support files for MSP430 series       | [link](http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/exports/msp430-gcc-support-files-1.194.zip)
v0.24.tar.gz                                        | MSPDebug distribution release archive | [link](https://github.com/dlbeer/mspdebug)
msp430.dll_developer_package_rev_3.08.001.000.zip   | msp430.so library (tilib)             | [link](http://www.ti.com/tool/mspds)

### Update

More up to date versions of the mentionned files may be available from TI.
But a modification of both the Makefile and the Dockerfile is required to
take such an update into account.

Especially for the mspdebug program. It is currently necessary to download
a release of mspdebug at version >= 0.24 to handle the MSP430FR4133. This
release may be replaced with a packaged version from the Debian repositories
in the future, when a proper version is available.

### Installation

To build the Docker image, extract the prerequisite files into a directory
named "vendor" until you get the following tree:

```
.
+-- Dockerfile
+-- Makefile
+-- README.md
+-- vendor
    +-- msp430.dll_developer_package_rev_3.08.001.000.zip
    +-- msp430-gcc-5.3.0.224_linux64.tar.bz2
    +-- msp430-gcc-support-files-1.194.zip
    +-- mspdebug
```

Then simply run:

```
make
```

## Test

To explore the created Docker image, you can run a shell in a new
container with:

```
make run
```
