############################################################
# Dockerfile to build MSP430FR4133 toolchain
# Based on Debian
############################################################

# Image is based on good old Debian
FROM debian:jessie

# My name is Corentin <corentin@sogilis.com>
MAINTAINER Corentin

# Workdir
ENV WORK /opt
ENV LIB430 ${WORK}/lib
ENV INCLUDE430 ${WORK}/include

ENV GCC_BZ2 msp430-gcc-5.3.0.224_linux64.tar.bz2
ENV GCC ${WORK}/msp430-gcc-5.3.0.224_linux64
ENV MSPDEBUG_BIN mspdebug
ENV SUPPORT_ZIP msp430-gcc-support-files-1.194.zip
ENV SUPPORT msp430-gcc-support-files
ENV SUPPORT_INC ${SUPPORT}/include
ENV MSPDEBUGSTACK_ZIP msp430.dll_developer_package_rev_3.08.001.000.zip
ENV MSPDEBUGSTACK mspdebugstack

# Install MSP430-GCC from TI
COPY vendor/${GCC_BZ2} .
COPY vendor/${SUPPORT_ZIP} .
COPY vendor/${MSPDEBUG_BIN} .
COPY vendor/${MSPDEBUGSTACK_ZIP} .

# Install mspdebug plus tools to unzip archives
RUN apt-get update                                                                    && \
    apt-get install -y --no-install-recommends lbzip2 unzip                           && \
    tar xvf ${GCC_BZ2} -C ${WORK}                                                     && \
    unzip ${SUPPORT_ZIP}                                                              && \
    mkdir -p ${LIB430} ${INCLUDE430}                                                  && \
    cp ${SUPPORT_INC}/msp430fr4133*.ld ${LIB430}                                      && \
    cp ${SUPPORT_INC}/in430.h ${INCLUDE430}                                           && \
    cp ${SUPPORT_INC}/iomacros.h ${INCLUDE430}                                        && \
    cp ${SUPPORT_INC}/msp430fr4133.h ${INCLUDE430}                                    && \
    cp ${SUPPORT_INC}/msp430.h ${INCLUDE430}                                          && \
    unzip ${MSPDEBUGSTACK_ZIP} -d ${MSPDEBUGSTACK}                                    && \
    cp ${MSPDEBUGSTACK}/libmsp430_64.so /usr/lib/libmsp430.so                         && \
    mv ${MSPDEBUG_BIN} /usr/bin/                                                      && \
    rm -rf ${GCC_BZ2} ${SUPPORT_ZIP} ${SUPPORT} ${MSPDEBUGSTACK_ZIP} ${MSPDEBUGSTACK} && \
    apt-get remove -y lbzip2 unzip

# Configure headers and linker files
ENV PATH ${GCC}/bin:$PATH

# Set workdir
WORKDIR ${WORK}/workspace
